/**
 * Copyright (c) 2012 - 2017 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.eclipselabs.osgi.jersey;

import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.ws.rs.core.Application;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.eclipselabs.osgi.jaxrs.SampleAdminApplication;
import org.eclipselabs.osgi.jaxrs.SampleApplication;
import org.eclipselabs.osgi.jaxrs.helper.JaxRsHelper;
import org.eclipselabs.osgi.jaxrs.provider.PrototypeResourceProvider;
import org.eclipselabs.osgi.jersey.binder.PrototypeServiceBinder;
import org.eclipselabs.osgi.jersey.jetty.JettyServerRunnable;
import org.eclipselabs.osgi.jersey.provider.JerseyResourceInstanceFactory;
import org.glassfish.hk2.api.Factory;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.servlet.ServletContainer;
import org.osgi.framework.BundleContext;
import org.osgi.service.component.ComponentContext;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.log.LogService;

/**
 * OSGi component to start a Jersey server instances. The server is started on component activation, 
 * resp. when activate is called. The server will be shut down on component deactivation, resp. when deactivate is called.
 * @author Mark Hoffmann
 * @since 12.07.2017
 */
//@Component(name="JerseyServer", immediate=true)
public class JerseyComponent {

	@Reference
	private LogService logger;
	
	@Reference
	private JerseyFactoryProvider factoryProvider;

	@Reference(target="(osgi.jaxrs.application=*)")
	private Application jaxrsOSGiApplication;
	
	private volatile Server server;
	private volatile PrototypeServiceBinder osgiBinder;
	private volatile Server adminServer;
	private volatile ComponentContext ctx;

	/**
	 * Called on component activation
	 * @param ctx the component context
	 */
	@Activate
	public void activate(ComponentContext ctx) {
		this.ctx = ctx;
		this.osgiBinder = new PrototypeServiceBinder();
		ExecutorService serverExecutor = Executors.newFixedThreadPool(2);
		System.out.println("Start Jersey server at port 8182");
		
		SampleApplication jaxrsApplication = new SampleApplication();
		server = new Server(8182);
		ServletContextHandler context = new ServletContextHandler(server, "/");
		registerApplication(context, jaxrsApplication);
		if (jaxrsOSGiApplication != null) {
			registerApplication(context, jaxrsOSGiApplication);
		}
		serverExecutor.submit(new JettyServerRunnable(server, 8182));
		System.out.println("Started Jersey server at port 8182");
		
		
		System.out.println("Start Jersey admin server at port 8183");
		adminServer = new Server(8183);
		ServletContextHandler adminContext = new ServletContextHandler(adminServer, "/admin");
		SampleAdminApplication jaxrsAdminApplication = new SampleAdminApplication();
//		registerApplication(adminContext, jaxrsAdminApplication);
		serverExecutor.submit(new JettyServerRunnable(adminServer, 8183));
		System.out.println("Started Jersey admin server at port 8183");
	}

	/**
	 * Called on component deactivation. It stops the servers.
	 */
	@Deactivate
	public void deactivate() {
		if (osgiBinder != null) {
			osgiBinder.dispose();
		}
		System.out.println("Stop Jersey server");
		logger.log(LogService.LOG_INFO, "Stop Jersey server");
		if (server != null && server.isRunning()) {
			try {
				server.stop();
			} catch (Exception e) {
				System.out.println("Error stopping Jersey server " + e.getMessage());
			}
		}
		System.out.println("Stop Jersey admin server");
		logger.log(LogService.LOG_INFO, "Stop Jersey admin server");
		if (adminServer != null && adminServer.isRunning()) {
			try {
				adminServer.stop();
			} catch (Exception e) {
				System.out.println("Error stopping Jersey admin server " + e.getMessage());
			}
		}
	}
	
	/**
	 * Registers a JaxRs {@link Application} to the Jersey servlet
	 * @param contextHandler the {@link ServletContextHandler}
	 * @param application the JaxRs application
	 */
	private void registerApplication(ServletContextHandler contextHandler, Application application) {
		if (contextHandler == null) {
			throw new IllegalArgumentException("No context handler is available to register an JaxRs application");
		}
		if (application == null) {
			logger.log(LogService.LOG_WARNING, "Cannot register an null application to the context handler for path " + contextHandler.getContextPath());
			return;
		}
		ResourceConfig config = ResourceConfig.forApplication(application);
		// prepare factory creation to forward prototype functionality to Jersey
		if (application instanceof PrototypeResourceProvider) {
			System.out.println("Register a prototype provider like application");
			if (ctx == null) {
				throw new IllegalStateException("Cannot create prototype factories without component context");
			}
			PrototypeResourceProvider prp = (PrototypeResourceProvider) application;
			BundleContext bctx = ctx.getBundleContext();
			Set<Class<?>> classes = prp.getPrototypeResourceClasses();
			classes.forEach((c)->{
				Factory<?> factory = new JerseyResourceInstanceFactory<>(bctx, c);
				osgiBinder.register(c, factory);
			});
			config.register(osgiBinder);
		}
		ServletHolder servlet = new ServletHolder(new ServletContainer(config));
		String applicationPath = JaxRsHelper.getServletPath(application);
		contextHandler.addServlet(servlet, applicationPath);
	}

}
