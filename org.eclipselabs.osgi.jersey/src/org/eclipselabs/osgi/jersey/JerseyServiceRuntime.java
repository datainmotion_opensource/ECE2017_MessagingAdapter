/**
 * Copyright (c) 2012 - 2017 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.eclipselabs.osgi.jersey;

import java.io.ObjectInputValidation;
import java.net.URI;
import java.util.Dictionary;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicLong;

import javax.ws.rs.core.Application;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipselabs.osgi.jaxrs.helper.JaxRsHelper;
import org.eclipselabs.osgi.jersey.binder.PrototypeServiceBinder;
import org.eclipselabs.osgi.jersey.dto.JerseyApplicationDTO;
import org.eclipselabs.osgi.jersey.jetty.JettyServerRunnable;
import org.glassfish.jersey.server.ResourceConfig;
import org.osgi.framework.Bundle;
import org.osgi.framework.Constants;
import org.osgi.framework.ServiceReference;
import org.osgi.framework.ServiceRegistration;
import org.osgi.framework.dto.ServiceReferenceDTO;
import org.osgi.namespace.implementation.ImplementationNamespace;
import org.osgi.service.cm.ConfigurationException;
import org.osgi.service.component.ComponentContext;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ConfigurationPolicy;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Modified;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.jaxrs.runtime.JaxRSServiceRuntime;
import org.osgi.service.jaxrs.runtime.JaxRSServiceRuntimeConstants;
import org.osgi.service.jaxrs.runtime.dto.ApplicationDTO;
import org.osgi.service.jaxrs.runtime.dto.RequestInfoDTO;
import org.osgi.service.jaxrs.runtime.dto.RuntimeDTO;
import org.osgi.service.log.LogService;

import aQute.bnd.annotation.headers.ProvideCapability;

import static org.eclipselabs.osgi.jersey.JerseyConstants.*;

/**
 * Implementation of the {@link JaxRSServiceRuntime} for a Jersey implementation
 * @author Mark Hoffmann
 * @since 12.07.2017
 */
@ProvideCapability(ns = ImplementationNamespace.IMPLEMENTATION_NAMESPACE, 
	version="1.0", 
	value = "osgi.implementation=\"osgi.jaxrs\"", 
	uses= {"javax.ws.rs", "javax.ws.rs.client", "javax.ws.rs.container", "javax.ws.rs.core", "javax.ws.rs.ext", "org.osgi.service.jaxrs.whiteboard"})
@Component(configurationPolicy=ConfigurationPolicy.REQUIRE, name="JerseyServiceRuntime", immediate=true, service=JaxRSServiceRuntime.class)
public class JerseyServiceRuntime implements JaxRSServiceRuntime {
	
	private volatile AtomicLong changeCount = new AtomicLong();
	private volatile PrototypeServiceBinder binder;
	private volatile Server jettyServer;
	private volatile ServletContextHandler contextHandler;
	private volatile RuntimeDTO runtimeDTO = new RuntimeDTO();
	private Integer port = WHITEBOARD_DEFAULT_PORT;
	private String contextPath = WHITEBOARD_DEFAULT_CONTEXT_PATH;
	private ComponentContext context;
	private ResourceConfig defaultApplication = null;
	@Reference
	private LogService log;

	/* 
	 * (non-Javadoc)
	 * @see org.osgi.service.jaxrs.runtime.JaxRSServiceRuntime#getRuntimeDTO()
	 */
	@Override
	public RuntimeDTO getRuntimeDTO() {
		if (context != null) {
			ServiceReference<?> serviceRef = context.getServiceReference();
			ServiceReferenceDTO srDTO = toServiceReferenceDTO(serviceRef);
			runtimeDTO.serviceDTO = srDTO;
		}
		return runtimeDTO;
	}

	/* 
	 * (non-Javadoc)
	 * @see org.osgi.service.jaxrs.runtime.JaxRSServiceRuntime#calculateRequestInfoDTO(java.lang.String)
	 */
	@Override
	public RequestInfoDTO calculateRequestInfoDTO(String path) {
		RequestInfoDTO ridto = new RequestInfoDTO();
		return ridto;
	}
	
	/**
	 * Called on component activation
	 * @param ctx the component context
	 * @throws ConfigurationException 
	 */
	@Activate
	public void activate(ComponentContext ctx) throws ConfigurationException {
		this.context = ctx;
		updateProperties(context);
		startServerAndContext();
	}
	
	/**
	 * Called on component de-activation
	 * @param ctx the component context
	 */
	@Deactivate
	public void deactivate() {
		changeCount.set(0);
		stopContextHandler();
		stopServer();
		binder.dispose();
	}
	
	/**
	 * Called on component modification
	 * @param ctx the component context
	 * @throws ConfigurationException thrown if the configuration is not valid 
	 */
	@Modified
	public void modify(ComponentContext ctx) throws ConfigurationException {
		Integer oldPort = port;
		String oldContextPath = contextPath;
		updateProperties(ctx);
		boolean portChanged = !this.port.equals(oldPort);
		boolean pathChanged = !this.contextPath.equals(oldContextPath);
		
		if (!pathChanged && !portChanged) {
			return;
		}
		// if only the path has changed, we only need to restart the context handler
		if (pathChanged && !portChanged) {
			restartContextHandler();
		}
		// if port changed, both parts need to be restarted, no matter, if the context path has changed
		if (portChanged) {
			stopContextHandler();
			stopServer();
			startServerAndContext();
		}
		long changedCnt = changeCount.incrementAndGet();
		ctx.getProperties().put("service.changecount", Long.valueOf(changedCnt));
	}
	
	/**
	 * Updates the fields that are provided by service properties.
	 * @param ctx the component context
	 * @throws ConfigurationException thrown when no context is available or the expected property was not provided 
	 */
	private void updateProperties(ComponentContext ctx) throws ConfigurationException {
		if (ctx == null) {
			throw new ConfigurationException(JaxRSServiceRuntimeConstants.JAX_RS_SERVICE_ENDPOINT, "No component context is availble to get properties from");
		}
		String[] urls = getPropertyWithDefault(ctx, JaxRSServiceRuntimeConstants.JAX_RS_SERVICE_ENDPOINT, new String[0]);
		if (urls.length == 0) {
			throw new ConfigurationException(JaxRSServiceRuntimeConstants.JAX_RS_SERVICE_ENDPOINT, "No endpoint definition was provided, but is mandatory");
		}
		URI[] uris = new URI[urls.length];
		for (int i = 0; i < urls.length; i++) {
			uris[i] = URI.create(urls[i]);
		}
		URI uri = uris[0];
		if (uri.getPort() > 0) {
			port = uri.getPort();
		}
		if (uri.getPath() != null) {
			contextPath = uri.getPath();
		}
	}

    /**
     * This mapping sequence was taken from:
     * @see https://github.com/njbartlett/osgi_jigsaw/blob/master/nbartlett-jigsaw-osgi/src/nbartlett.jigsaw_osgi/org/apache/felix/framework/DTOFactory.java
     * @param svc the service reference
     * @return the service reference dto
     */
    private ServiceReferenceDTO toServiceReferenceDTO(ServiceReference<?> svc) {
        ServiceReferenceDTO dto = new ServiceReferenceDTO();
        dto.bundle = svc.getBundle().getBundleId();
        dto.id = (Long) svc.getProperty(Constants.SERVICE_ID);
        Map<String, Object> props = new HashMap<String, Object>();
        for (String key : svc.getPropertyKeys()) {
            props.put(key, svc.getProperty(key));
        }
        dto.properties = new HashMap<String, Object>(props);

        Bundle[] ubs = svc.getUsingBundles();
        if (ubs == null)
        {
            dto.usingBundles = new long[0];
        }
        else
        {
            dto.usingBundles = new long[ubs.length];
            for (int j=0; j < ubs.length; j++)
            {
                dto.usingBundles[j] = ubs[j].getBundleId();
            }
        }
        return dto;
    }
    
    /**
     * This mapping sequence was taken from:
     * @see https://github.com/njbartlett/osgi_jigsaw/blob/master/nbartlett-jigsaw-osgi/src/nbartlett.jigsaw_osgi/org/apache/felix/framework/DTOFactory.java
     * @param svc the service reference
     * @return the service reference dto
     */
    private ApplicationDTO toApplicationDTO(Application application, String name) {
    	if (application == null || name == null) {
    		throw new IllegalArgumentException("Expected an application and/or name parameter to create an ApplicationDTO");
    	}
    	ApplicationDTO dto = new JerseyApplicationDTO();
    	dto.name = name;
    	dto.base = JaxRsHelper.getServletPath(application);
    	return dto;
    }
    
    /**
     * Returns the property. If it not available but a default value is set, the
     * default value will be returned.
     * @param context the component context
     * @param key the properties key
     * @param defaultValue the default value
     * @return the value or defaultValue or <code>null</code>
     */
    @SuppressWarnings("unchecked")
	private static <T> T getPropertyWithDefault(ComponentContext context, String key, T defaultValue) {
    	if (context == null) {
    		throw new IllegalStateException("Cannot call getProperties in a state, where the component context is not available");
    	}
    	Object value = context.getProperties().get(key);
    	return value == null ? defaultValue : (T)value;
    }
    
    /**
     * Restart the context handler only
     */
    private void restartContextHandler() {
    	if (!jettyServer.isRunning()) {
    		log.log(LogService.LOG_INFO, "Restarting JaxRs white-board with a not running server");
    		startServerAndContext();
    		return;
    	}
    	if (binder != null) {
    		binder.dispose();
    	}
    	stopContextHandler();
    	binder = new PrototypeServiceBinder();
    	try {
    		contextHandler = new ServletContextHandler(jettyServer, contextPath);
    		log.log(LogService.LOG_INFO, "Restarted JaxRs white-board with existing server and new contextPath: " + contextPath);
    	} catch (Exception e) {
    		log.log(LogService.LOG_ERROR, "Error restarting JaxRs white-board because of an exception", e);
    	}
    	
    }
    
    /**
     * Starts the Jetty server and initializes the current context handler
     */
    private void startServerAndContext() {
    	if (binder != null) {
    		binder.dispose();
    	}
    	binder = new PrototypeServiceBinder();
		try {
			if (jettyServer != null && !jettyServer.isStopped()) {
				log.log(LogService.LOG_WARNING, "Stopping JaxRs white-board server on startup, but it wasn't exepected to run");
				stopContextHandler();
				stopServer();
			}
			jettyServer = new Server(port);
			contextHandler = new ServletContextHandler(jettyServer, contextPath);
			Executors.newSingleThreadExecutor().submit(new JettyServerRunnable(jettyServer, port));
			log.log(LogService.LOG_INFO, "Started JaxRs white-board server and context handler for port: " + port + " and context: " + contextPath);
		} catch (Exception e) {
			log.log(LogService.LOG_ERROR, "Error starting JaxRs white-board because of an exception", e);
		}
    }
    
    /**
     * Stopps the Jetty context handler;
     */
    private void stopContextHandler() {
    	if (contextHandler == null) {
    		log.log(LogService.LOG_WARNING, "Try to stop Jetty context handler, but there is none");
    		return;
    	}
    	if (contextHandler.isStopped()) {
    		log.log(LogService.LOG_WARNING, "Try to stop Jetty context handler, but it was already stopped");
    		return;
    	}
    	try {
    		contextHandler.stop();
    		contextHandler.destroy();
    		contextHandler = null;
    	} catch (Exception e) {
    		log.log(LogService.LOG_ERROR, "Error stopping Jetty context handler", e);
    	}
    }
    
    /**
     * Stopps the Jetty server;
     */
    private void stopServer() {
    	if (jettyServer == null) {
    		log.log(LogService.LOG_WARNING, "Try to stop JaxRs whiteboard server, but there is none");
    		return;
    	}
    	if (jettyServer.isStopped()) {
    		log.log(LogService.LOG_WARNING, "Try to stop JaxRs whiteboard server, but it was already stopped");
    		return;
    	}
    	try {
    		jettyServer.stop();
    		jettyServer.destroy();
    		jettyServer = null;
    	} catch (Exception e) {
    		log.log(LogService.LOG_ERROR, "Error stopping Jetty server", e);
    	}
    }
	
}
