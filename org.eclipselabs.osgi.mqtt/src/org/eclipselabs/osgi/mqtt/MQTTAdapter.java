/**
 * Copyright (c) 2012 - 2017 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.eclipselabs.osgi.mqtt;

import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.MqttSecurityException;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ConfigurationPolicy;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Modified;
import org.osgi.service.mqtt.MQTTMessage;
import org.osgi.service.mqtt.MQTTService;
import org.osgi.service.mqtt.QoS;
import org.osgi.util.pushstream.PushStream;
import org.osgi.util.pushstream.PushStreamProvider;
import org.osgi.util.pushstream.PushbackPolicyOption;
import org.osgi.util.pushstream.QueuePolicyOption;

/**
 * Sample implementation of the OSGi RFC229
 * @author Mark Hoffmann
 * @since 19.03.2017
 */
@Component(service=MQTTService.class, name="MQTTService", configurationPolicy=ConfigurationPolicy.REQUIRE, immediate=true)
public class MQTTAdapter implements MQTTService {

	private static Logger LOG = Logger.getLogger("org.eclipselabs.osgi.mqtt.mqttAdapter");
	private final AtomicReference<MqttClient> clientRef = new AtomicReference<>();
	private volatile List<String> mqttSubscribeTopics = null;
	private volatile List<String> mqttPublishTopics = new LinkedList<>();
	private volatile MqttConnectOptions options = null;
	private volatile MQTTCallbackProxy callbackProxy = null;
	private final PushStreamProvider psProvider = new PushStreamProvider();

	/* (non-Javadoc)
	 * @see org.osgi.service.mqtt.MQTTService#subscribe(java.lang.String)
	 */
	@Override
	public PushStream<MQTTMessage> subscribe(String topic) {
		if (callbackProxy == null) {
			throw new IllegalStateException("No callback proxy is available to create a push stream");
		}
		if (!mqttSubscribeTopics.contains(topic)) {
			throw new IllegalArgumentException("Topic " + topic + " is not part of the list: " + mqttSubscribeTopics);
		}
		MQTTEventSource eventSource = new MQTTEventSource(callbackProxy, topic);
		return psProvider.buildStream(eventSource)
				.withPushbackPolicy(PushbackPolicyOption.LINEAR, 20)
				.withQueuePolicy(QueuePolicyOption.FAIL)
				.create().filter(msg->msg.topic().equals(topic));
	}

	/* (non-Javadoc)
	 * @see org.osgi.service.mqtt.MQTTService#subscribe(java.lang.String, org.osgi.service.mqtt.QoS)
	 */
	@Override
	public PushStream<MQTTMessage> subscribe(String topic, QoS qos) {
		return subscribe(topic);
	}

	/* (non-Javadoc)
	 * @see org.osgi.service.mqtt.MQTTService#publish(java.lang.String, java.nio.ByteBuffer)
	 */
	@Override
	public void publish(String topic, ByteBuffer content) throws Exception {
		publish(topic, content, null);
	}

	/* (non-Javadoc)
	 * @see org.osgi.service.mqtt.MQTTService#publish(java.lang.String, java.nio.ByteBuffer, org.osgi.service.mqtt.QoS)
	 */
	@Override
	public void publish(String topic, ByteBuffer content, QoS qos) throws Exception {
		if (!validateTopic(topic)) {
			throw new IllegalArgumentException("The topic '" + topic + "' is not registered to be sent from this client");
		}
		byte[] payload = content.array();
		if (clientRef.get() != null) {
			MqttMessage message = new MqttMessage(payload);
			if (qos != null) {
				message.setQos(qos.ordinal());
			}
			clientRef.get().publish(topic, message);
		}
	}

	/* (non-Javadoc)
	 * @see org.osgi.service.mqtt.MQTTService#publishRetained(java.lang.String, java.nio.ByteBuffer)
	 */
	@Override
	public void publishRetained(String topic, ByteBuffer content) throws Exception {
		byte[] payload = content.array();
		if (clientRef.get() != null) {
			MqttMessage message = new MqttMessage(payload);
			message.setRetained(true);
			clientRef.get().publish(topic, message);
		}
	}

	/**
	 * Activate method, that is called, when the component was activated
	 * @param properties the properties to configure the component
	 */
	@Activate
	public void activate(Map<String, String> properties) {
		String pid = properties.getOrDefault("service.pid", MqttClient.generateClientId());
		// TODO Recommend to have multiple server URL's for the client, separated by colon
		String serverUrl = properties.getOrDefault(PROP_BROKER, "tcp://localhost:1883");
		String[] serverUrls = serverUrl.split(",");
		Object p = properties.get(PROP_MQTT_PUBLISH_ON_SUBSCRIBE);
		boolean publishOnSubscribe = false;
		if (p != null && p instanceof Boolean) {
			publishOnSubscribe = ((Boolean)p).booleanValue();
		}
		String mqttSubscribeTopicString = properties.getOrDefault(PROP_MQTT_SUBSCRIBE_TOPICS, "");
		mqttSubscribeTopics = mqttSubscribeTopicString.isEmpty() ? Collections.emptyList() : Arrays.asList(mqttSubscribeTopicString.split(","));
		String mqttPublishTopicString = properties.getOrDefault(PROP_MQTT_PUBLISH_TOPICS, "");
		List<String> publishToAdd = mqttPublishTopicString.isEmpty() ? new LinkedList<>() : Arrays.asList(mqttPublishTopicString.split(","));
		mqttPublishTopics.addAll(publishToAdd);
		if (publishOnSubscribe) {
			mqttPublishTopics.addAll(mqttSubscribeTopics);
		}
		options = new MqttConnectOptions();
		options.setServerURIs(serverUrls);
		String clientId = serverUrl;
		try {
			MqttClient client = new MqttClient(serverUrls[0], pid);
			clientId = client.getClientId();
			MqttClient oldClient = clientRef.getAndSet(client);
			if (oldClient != null) {
				oldClient.close();
			}
			client.connect(options);
			if (callbackProxy == null) {
				callbackProxy = new MQTTCallbackProxy(client, mqttSubscribeTopics);
				client.setCallback(callbackProxy);
			}
		} catch (MqttSecurityException e) {
			LOG.log(Level.SEVERE, "[" + clientId + "] Cannot establish connection with MQTT client to URL " + serverUrl + " because of security issues", e);
		} catch (MqttException e) {
			LOG.log(Level.SEVERE, "[" + clientId + "] Cannot establish connection with MQTT client to URL " + serverUrl, e);
		}

	}

	/**
	 * De-activate method, that is called, when the component was de-activated.
	 * The callback proxy is closed first, to be able to send CLOSE events to the push stream
	 * @param properties the properties to configure the component
	 */
	@Deactivate
	public void deactivate(Map<String, String> properties) {
		if (callbackProxy != null) {
			try {
				callbackProxy.dispose();
			} catch (Exception e) {
				LOG.log(Level.SEVERE, "Cannot dispose MQTTCallbackProxy", e);
			}
		}
		MqttClient client = clientRef.getAndSet(null);
		if (client != null) {
			try {
				if (client.isConnected()) {
					client.disconnect();
				}
				client.close();
			} catch (MqttException e) {
				LOG.log(Level.SEVERE, "Cannot disconnect and close MQTTClinet", e);
			}
		}
	}

	/**
	 * Modify method, that is called, when the component was modified
	 * @param properties the properties to configure the component
	 */
	@Modified
	public void modify(Map<String, String> properties) {
		deactivate(properties);
		activate(properties);
	}
	
	/**
	 * Validates if the topic is registered or fits into a wildcard topic
	 * @param topic the topic to be checked
	 * @return <code>true</code>, if the topic is valid
	 */
	private boolean validateTopic(String topic) {
		if (topic == null || topic.isEmpty() || topic.contains("*")) {
			return false;
		}
		if (mqttPublishTopics.contains(topic)) {
			return true;
		}
		return mqttPublishTopics.stream().
				filter((t)->t.endsWith("*") && 
						topic.startsWith(t.substring(0, t.length() - 2))).
				findFirst().
				isPresent();
	}
}
