/**
 * 
 */
package org.eclipselabs.osgi.mqtt.command;

import java.io.IOException;
import java.util.Dictionary;
import java.util.Hashtable;

import org.osgi.service.cm.Configuration;
import org.osgi.service.cm.ConfigurationAdmin;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.mqtt.MQTTService;

/**
 * @author mark
 *
 */
@Component(
		property={
				"osgi.command.scope=MQTTAdapter", 
				"osgi.command.function=createMQTTAdapter"
		},
		service = MqttAdapterCommand.class
	)
public class MqttAdapterCommand {

	@Reference
	private ConfigurationAdmin configAdmin;

	/**
	 * @param ci
	 */
	public void createMQTTAdapter(String[] args) {
		String host = null;
		String publishTopics = null;
		if (args != null) {
			for (int i = 0; i < args.length; i++) {
				switch (i) {
				case 0:
					host = args[i];
					break;
				case 1:
					publishTopics = args[i];
				default:
					break;
				}
			}
		}
		try {
			Configuration clientConfig = configAdmin.getConfiguration("MQTTService", "?");
			// has to be a new configuration
			Dictionary<String, Object> p = clientConfig.getProperties();
			// add service properties
			p = new Hashtable<>();
			if (publishTopics != null) {
				p.put(MQTTService.PROP_MQTT_PUBLISH_TOPICS, publishTopics);
			}
			if (host != null) {
				p.put(MQTTService.PROP_BROKER, host);
			}
			clientConfig.update(p);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
