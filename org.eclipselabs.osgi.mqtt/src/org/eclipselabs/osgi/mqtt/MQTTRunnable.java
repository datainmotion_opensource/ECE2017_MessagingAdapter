/**
 * Copyright (c) 2012 - 2017 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.eclipselabs.osgi.mqtt;

import java.util.concurrent.CountDownLatch;

/**
 * Runnable that just runs and wait
 * @author Mark Hoffmann
 * @since 17.04.2017
 */
public class MQTTRunnable implements Runnable {

	private CountDownLatch workingLatch = null;

	public MQTTRunnable() {
	}
	
	/* 
	 * (non-Javadoc)
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {
		try {
			if (workingLatch == null) {
				workingLatch = new CountDownLatch(1);
			} else {
				throw new IllegalStateException("Push stream was already opened");
			}
			workingLatch.await();
		} catch (InterruptedException  e) {
			workingLatch.countDown();
		}
	}

}
