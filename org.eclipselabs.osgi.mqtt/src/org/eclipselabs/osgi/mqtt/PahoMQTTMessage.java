/**
 * Copyright (c) 2012 - 2017 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.eclipselabs.osgi.mqtt;

import java.nio.ByteBuffer;

import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.osgi.service.mqtt.MQTTMessage;

/**
 * Paho specific {@link MQTTMessage}
 * @author Mark Hoffmann
 * @since 19.03.2017
 */
public class PahoMQTTMessage implements MQTTMessage {
	
	private final String topic;
	private final ByteBuffer payload;

	private PahoMQTTMessage(String topic, ByteBuffer payload) {
		this.topic = topic;
		this.payload = payload;
	}

	/* 
	 * (non-Javadoc)
	 * @see org.osgi.service.mqtt.MQTTMessage#topic()
	 */
	@Override
	public String topic() {
		return topic;
	}

	/* 
	 * (non-Javadoc)
	 * @see org.osgi.service.mqtt.MQTTMessage#payload()
	 */
	@Override
	public ByteBuffer payload() {
		return payload;
	}
	
	/**
	 * Converts a Paho {@link MqttMessage} into an own one
	 * @param msg the original message
	 * @param topic the topic
	 * @return the converted message
	 */
	public static MQTTMessage fromPahoMessage(MqttMessage msg, String topic) {
		ByteBuffer content = ByteBuffer.wrap(msg.getPayload());
		return new PahoMQTTMessage(topic, content);
	}

}
