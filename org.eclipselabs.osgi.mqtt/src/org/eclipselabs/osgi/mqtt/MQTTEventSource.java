/**
 * Copyright (c) 2012 - 2017 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.eclipselabs.osgi.mqtt;

import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicReference;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.osgi.service.mqtt.MQTTMessage;
import org.osgi.util.pushstream.PushEvent;
import org.osgi.util.pushstream.PushEventConsumer;
import org.osgi.util.pushstream.PushEventSource;

/**
 * Pushstream event source for {@link MqttMessage} 
 * @author Mark Hoffmann
 * @since 16.04.2017
 */
public class MQTTEventSource implements PushEventSource<MQTTMessage>, MqttCallback {

	private static Logger LOG = Logger.getLogger("org.eclipselabs.osgi.mqtt.mqttEventSource");
	private volatile PushEventConsumer<? super MQTTMessage> consumer = null;
	private volatile Future<?> future = null;
	private final AtomicReference<MQTTCallbackProxy> proxyRef;
	private final String topic;

	public MQTTEventSource(MQTTCallbackProxy proxy, String topic) {
		this.proxyRef = new AtomicReference<MQTTCallbackProxy>(proxy);
		this.topic = topic;
	}
	
	/* 
	 * (non-Javadoc)
	 * @see org.osgi.util.pushstream.PushEventSource#open(org.osgi.util.pushstream.PushEventConsumer)
	 */
	@Override
	public AutoCloseable open(PushEventConsumer<? super MQTTMessage> messageConsumer) throws Exception {
		if (future == null) {
			MQTTCallbackProxy proxy = proxyRef.get();
			boolean added = proxy.addCallbackListener(topic, this);
			if (!added) {
				throw new IllegalStateException("[" + topic + "] Cannot add MQTT event source callback to MQTTCallbackProxy");
			}
			consumer = messageConsumer;
			future = Executors.newSingleThreadExecutor().submit(new MQTTRunnable());
		} else {
			LOG.log(Level.SEVERE, "[" + topic + "] Push-stream is already open for this topic");
		}
		return () -> { 
			close(); 
		};
	}

	/**
	 * Closes the event source
	 * @throws Exception
	 */
	public void close() throws Exception {
		if (consumer != null) {
			consumer.accept(PushEvent.close());
		}
		if (proxyRef != null) {
			MQTTCallbackProxy proxy = proxyRef.get();
			if (!proxy.removeCallbackListener(topic, this)) {
				LOG.log(Level.WARNING, "[" + topic + "] Cannot remove MQTT callback from MQTTCallbackProxy for topic: " + topic);
			}
		}
		if (future != null && 
				!future.isDone() && 
				!future.isCancelled()) {
			if (!future.cancel(true)) {
				LOG.log(Level.WARNING, "[" + topic + "] Cannot cancel MQTT callback from MQTTCallbackProxy for topic: " + topic);

			}
		}
	}

	/* 
	 * (non-Javadoc)
	 * @see org.eclipse.paho.client.mqttv3.MqttCallback#connectionLost(java.lang.Throwable)
	 */
	@Override
	public void connectionLost(Throwable cause) {
		if (consumer == null) {
			return;
		}
		Exception exception = (Exception) cause;
		try {
			consumer.accept(PushEvent.error(exception));
		} catch (Exception e) {
			LOG.log(Level.SEVERE, "[" + topic + "] Error sending push even error to message consumer", e);
		}
	}

	/* 
	 * (non-Javadoc)
	 * @see org.eclipse.paho.client.mqttv3.MqttCallback#deliveryComplete(org.eclipse.paho.client.mqttv3.IMqttDeliveryToken)
	 */
	@Override
	public void deliveryComplete(IMqttDeliveryToken token) {
		LOG.log(Level.WARNING, "[" + topic + "] Received delivery completion but cannot send it, because it is not implemented");
	}

	/* 
	 * (non-Javadoc)
	 * @see org.eclipse.paho.client.mqttv3.MqttCallback#messageArrived(java.lang.String, org.eclipse.paho.client.mqttv3.MqttMessage)
	 */
	@Override
	public void messageArrived(String topic, MqttMessage message) throws Exception {
		if (consumer == null) {
			return;
		}
		MQTTMessage msg = PahoMQTTMessage.fromPahoMessage(message, topic);
		consumer.accept(PushEvent.data(msg));
	}

}
