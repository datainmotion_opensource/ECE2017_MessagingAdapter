package test.api.spi;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ServiceScope;

import test.api.HelloService;

@Component(name="HelloPrototype2", service=HelloService.class, scope=ServiceScope.PROTOTYPE, property="test=2")
public class SayHelloPrototype2 implements HelloService {

	@Override
	public String sayHello() {
		String instance = this.toString();
		return "Hello prototype2 " + instance;
	}

}
