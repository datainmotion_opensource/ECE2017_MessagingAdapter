package test.api.spi;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ServiceScope;

import test.api.HelloService;

@Component(name="HelloPrototype", service=HelloService.class, scope=ServiceScope.PROTOTYPE, property="test=1")
public class SayHelloPrototype1 implements HelloService {

	@Override
	public String sayHello() {
		String instance = this.toString();
		return "Hello prototype " + instance;
	}

}
