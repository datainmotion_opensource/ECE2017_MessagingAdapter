package test.api.spi;

import java.util.List;

import org.osgi.service.component.ComponentServiceObjects;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;
import org.osgi.service.component.annotations.ReferenceScope;

import test.api.HelloService;

@Component(name="PrototypeConsumer")
public class SayHelloProtypeComponent {
	
	@Reference(cardinality=ReferenceCardinality.AT_LEAST_ONE, scope=ReferenceScope.PROTOTYPE_REQUIRED, target="(test=*)")
	private List<ComponentServiceObjects<HelloService>> helloServices;
	
	@Activate
	public void activate() {
		System.out.println("Activate prototype consumer " + helloServices.size());
		helloServices.forEach((f)->{
			HelloService s1 = f.getService();
			HelloService s2 = f.getService();
			System.out.println("Using factory " + f + ":");
			System.out.println("Creating services " + s1 + ", " + s2);
			f.ungetService(s1);
			f.ungetService(s2);
		});
//		HelloService s1 = helloService.getService();
//		HelloService s2 = helloService.getService();
//		System.out.println("Activate prototype consumer " + s1 + ", " + s2);
	}

}
