package org.eclipselabs.osgi.jaxrs;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;

@Component(name="OSGiSampleResource", service=Object.class, property="osgi.jaxrs.resource=org.eclipselabs.osgi.jaxrs.OSGiSampleResource")
@Path("osgi")
public class OSGiSampleResource {

	@GET
	@Path("{name}")
	@Produces(MediaType.TEXT_PLAIN)
	public Response helloOSGi(@PathParam("name") String name) {
		String hello = "Hello from OSGi Resource " + name;
		return Response.ok(hello).build();
	}
	
	@Activate
	public void activate() {
		System.out.println("Activate resource");
	}
}
