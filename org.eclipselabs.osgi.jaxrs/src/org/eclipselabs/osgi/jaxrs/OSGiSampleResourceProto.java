package org.eclipselabs.osgi.jaxrs;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ServiceScope;

@Component(name="OSGiSampleResourceProto", service=Object.class, property="osgi.jaxrs.resource=org.eclipselabs.osgi.jaxrs.OSGiSampleResourceProto", scope=ServiceScope.PROTOTYPE)
@Path("prototype")
public class OSGiSampleResourceProto {

	@GET
	@Path("{name}")
	@Produces(MediaType.TEXT_PLAIN)
	public Response helloOSGi(@PathParam("name") String name) {
		String hello = "Hello from OSGi prototype resource " + name;
		return Response.ok(hello).build();
	}
	
	@Activate
	public void activate() {
		System.out.println("Activate resource prototype");
	}
}
