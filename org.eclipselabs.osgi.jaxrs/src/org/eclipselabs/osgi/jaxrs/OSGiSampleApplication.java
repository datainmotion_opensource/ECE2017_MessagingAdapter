/**
 * Copyright (c) 2012 - 2017 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.eclipselabs.osgi.jaxrs;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

import org.eclipselabs.osgi.jaxrs.provider.PrototypeResourceProvider;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;
import org.osgi.service.component.annotations.ReferenceScope;

/**
 * A JaxRs application, registered as OSGi component.
 * It injects references to singleton services and prototype scoped services.
 * The {@link ApplicationPath} annotation is optional
 * @author Mark Hoffmann
 * @since 12.07.2017
 */
@Component(name="OSGiSampleApplication", service=Application.class, property="osgi.jaxrs.application=org.eclipselabs-osgi.jaxrs.OSGiSampleApplication")
@ApplicationPath("rest")
public class OSGiSampleApplication extends Application implements PrototypeResourceProvider {
	
	private volatile Set<Class<?>> classes = new HashSet<>();
	private volatile Set<Class<?>> prototypeClasses = new HashSet<>();
	private volatile Set<Object> singletons = new HashSet<>();
	// inject all singleton services in JaxRS scope
	@Reference(cardinality=ReferenceCardinality.MULTIPLE, target="(& (osgi.jaxrs.resource=*)(!(service.scope=prototype)))")
	private List<Object> resourceSingletonList;
	// inject all prototype services in 
	@Reference(cardinality=ReferenceCardinality.MULTIPLE, target="(osgi.jaxrs.resource=*)", scope=ReferenceScope.PROTOTYPE_REQUIRED)
	private List<Object> resourcePrototypeList;

	/* (non-Javadoc)
	 * @see javax.ws.rs.core.Application#getClasses()
	 */
	@Override
	public Set<Class<?>> getClasses() {
		return Collections.unmodifiableSet(classes);
	}
	
	/* 
	 * (non-Javadoc)
	 * @see javax.ws.rs.core.Application#getSingletons()
	 */
	@Override
	public Set<Object> getSingletons() {
		return Collections.unmodifiableSet(singletons);
	}
	
	/* 
	 * (non-Javadoc)
	 * @see org.eclipselabs.osgi.jaxrs.PrototypeResourceProvider#getPrototypeResourceClasses()
	 */
	@Override
	public Set<Class<?>> getPrototypeResourceClasses() {
		return Collections.unmodifiableSet(prototypeClasses);
	}

	/**
	 * Activate the application and fill the 
	 */
	@Activate
	public void activate() {
		System.out.println("Activate OSGiSampleApplication");
		classes.clear();
		prototypeClasses.clear();
		if (resourcePrototypeList != null) {
			resourcePrototypeList.forEach((sr)-> {
				Class<?> clazz = sr.getClass();
				classes.add(clazz);
				prototypeClasses.add(clazz);
			});
		}
		singletons.clear();
		if (resourceSingletonList != null) {
			singletons.addAll(resourceSingletonList);
		}
	}
	
	/**
	 * Deactivates the application and cleans up all resources
	 */
	@Deactivate
	public void deactivate() {
		classes.clear();
		singletons.clear();
	}
	
}
