package org.eclipselabs.osgi.jaxrs;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("hello")
@Produces(MediaType.TEXT_PLAIN)
public class SampleAdminResource {

	@GET
	@Path("/{name}")
	public Response sayHello(@PathParam("name") String name) {
		String hello = "Hello Admin " + name;
		return Response.ok(hello).build();
	}
}
