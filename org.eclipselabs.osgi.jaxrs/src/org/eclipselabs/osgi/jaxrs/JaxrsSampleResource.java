package org.eclipselabs.osgi.jaxrs;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ServiceScope;

@Component(service=Object.class, 
	property={"osgi.jaxrs.resource=true", "osgi.jaxrs.application.select=(osgi.jaxrs.name=jaxrsApp)"}, 
	scope=ServiceScope.PROTOTYPE)
@Path("jaxrs")
public class JaxrsSampleResource {

	@GET
	@Path("{name}")
	@Produces(MediaType.TEXT_PLAIN)
	public Response helloOSGi(@PathParam("name") String name) {
		String hello = "Hello from OSGi prototype resource " + name;
		return Response.ok(hello).build();
	}
	
	@Activate
	public void activate() {
		System.out.println("Activate resource jaxrs whiteboard prototype");
	}
}
