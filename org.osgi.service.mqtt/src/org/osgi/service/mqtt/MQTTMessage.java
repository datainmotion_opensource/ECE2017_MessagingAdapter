/*
 * Copyright (c) OSGi Alliance (2005, 2017). All Rights Reserved.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.osgi.service.mqtt;

import java.nio.ByteBuffer;

/**
 * An MQTT message representation
 * @author $Id$
 */
public interface MQTTMessage {
	
	/**
	 * Returns the MQTT topic, the message belongs to
	 * @return the MQTT topic, the message belongs to
	 */
	public String topic();
	
	/**
	 * Returns the playload of the message as {@link ByteBuffer}
	 * @return the playload of the message as {@link ByteBuffer}
	 */
	public ByteBuffer payload();

}
