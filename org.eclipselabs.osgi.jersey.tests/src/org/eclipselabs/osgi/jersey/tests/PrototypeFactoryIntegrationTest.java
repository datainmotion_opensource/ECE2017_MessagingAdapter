/**
 * Copyright (c) 2012 - 2017 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.eclipselabs.osgi.jersey.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.Collection;
import java.util.Dictionary;
import java.util.Hashtable;

import org.eclipselabs.osgi.jersey.provider.JerseyResourceInstanceFactory;
import org.eclipselabs.osgi.jersey.tests.services.ITest;
import org.eclipselabs.osgi.jersey.tests.services.TestImpl;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import org.osgi.framework.BundleContext;
import org.osgi.framework.Filter;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceReference;
import org.osgi.framework.ServiceRegistration;
import org.osgi.util.tracker.ServiceTracker;

/**
 * Tests the Jersey resource factory
 * @author Mark Hoffmann
 * @since 12.07.2017
 */
@RunWith(MockitoJUnitRunner.class)
public class PrototypeFactoryIntegrationTest {

	private final BundleContext context = FrameworkUtil.getBundle(PrototypeFactoryIntegrationTest.class).getBundleContext();

	@Before
	public void before() {
	}

	@After
	public void after() {
	}

	@Test(expected=IllegalStateException.class)
	public void testFactoryNoContext() {
		JerseyResourceInstanceFactory<?> factory = new JerseyResourceInstanceFactory<>(null, TestImpl.class);
		assertEquals(0, factory.getCacheInstanceCount());
		factory.provide();
		assertEquals(0, factory.getCacheInstanceCount());
	}
	
	@Test(expected=IllegalStateException.class)
	public void testFactoryNoClass() {
		JerseyResourceInstanceFactory<?> factory = new JerseyResourceInstanceFactory<>(context, null);
		assertEquals(0, factory.getCacheInstanceCount());
		factory.provide();
		assertEquals(0, factory.getCacheInstanceCount());
	}
	
	@Test(expected=IllegalStateException.class)
	public void testFactoryNoClassAndContext() {
		JerseyResourceInstanceFactory<?> factory = new JerseyResourceInstanceFactory<>(null, null);
		assertEquals(0, factory.getCacheInstanceCount());
		factory.provide();
		assertEquals(0, factory.getCacheInstanceCount());
	}
	
	@Test
	public void testFactoryDisposeNull() {
		JerseyResourceInstanceFactory<?> factory = new JerseyResourceInstanceFactory<>(null, null);
		assertEquals(0, factory.getCacheInstanceCount());
		factory.dispose(null);
		assertEquals(0, factory.getCacheInstanceCount());
	}
	
	@Test
	public void testFactoryPrototypeService() throws InvalidSyntaxException, InterruptedException {
		
		Filter filter = FrameworkUtil.createFilter("(osgi.jaxrs.resource=" + TestImpl.class.getName() + ")");
		Object service = getService(filter, 5000);
		assertNotNull(service);
		
		JerseyResourceInstanceFactory<TestImpl> factory = new JerseyResourceInstanceFactory<>(context, TestImpl.class);
		assertEquals(0, factory.getCacheInstanceCount());
		TestImpl i1 = factory.provide();
		assertNotNull(i1);
		assertEquals(1, factory.getCacheInstanceCount());
		TestImpl i2 = factory.provide();
		assertNotNull(i2);
		assertEquals(2, factory.getCacheInstanceCount());
		assertNotEquals(i1, i2);
		factory.dispose(i1);
		assertEquals(1, factory.getCacheInstanceCount());
		factory.dispose(i2);
		assertEquals(0, factory.getCacheInstanceCount());
	}
	
	@Test
	public void testFactoryPrototypeServiceWithRef() throws InvalidSyntaxException, InterruptedException {
		
		Filter filter = FrameworkUtil.createFilter("(osgi.jaxrs.resource=" + TestImpl.class.getName() + ")");
		Object service = getService(filter, 5000);
		assertNotNull(service);
		Collection<ServiceReference<Object>> references = context.getServiceReferences(Object.class, filter.toString());
		assertNotNull(references);
		assertEquals(1,  references.size());
		ServiceReference<Object> ref = references.iterator().next();
		JerseyResourceInstanceFactory<TestImpl> factory = new JerseyResourceInstanceFactory<>(context, TestImpl.class, ref);
		
		assertEquals(0, factory.getCacheInstanceCount());
		TestImpl i1 = factory.provide();
		assertNotNull(i1);
		assertEquals(1, factory.getCacheInstanceCount());
		TestImpl i2 = factory.provide();
		assertNotNull(i2);
		assertEquals(2, factory.getCacheInstanceCount());
		assertNotEquals(i1, i2);
		factory.dispose(i1);
		assertEquals(1, factory.getCacheInstanceCount());
		factory.dispose(i2);
		assertEquals(0, factory.getCacheInstanceCount());
	}
	
	@Test
	public void testFactoryNoPrototypeService() throws InvalidSyntaxException, InterruptedException {
		
		Dictionary<String, Object> properties = new Hashtable<>();
		properties.put("osgi.jaxrs.resource", ITest.class.getName());
		ServiceRegistration<Object> serviceRegistration = context.registerService(Object.class, new ITest() {
			
			@Override
			public String getString() {
				return "test";
			}
		}, properties);
		Filter filter = FrameworkUtil.createFilter("(osgi.jaxrs.resource=" + ITest.class.getName() + ")");
		Object service = getService(filter, 5000);
		assertNotNull(service);
		
		JerseyResourceInstanceFactory<ITest> factory = new JerseyResourceInstanceFactory<>(context, ITest.class);
		assertEquals(0, factory.getCacheInstanceCount());
		ITest i1 = factory.provide();
		assertNull(i1);
		assertEquals(0, factory.getCacheInstanceCount());
		serviceRegistration.unregister();
	}
	
	@Test
	public void testFactoryNoPrototypeServiceWithRef() throws InvalidSyntaxException, InterruptedException {
		
		Dictionary<String, Object> properties = new Hashtable<>();
		properties.put("osgi.jaxrs.resource", ITest.class.getName());
		ServiceRegistration<Object> serviceRegistration = context.registerService(Object.class, new ITest() {
			
			@Override
			public String getString() {
				return "test";
			}
		}, properties);
		Filter filter = FrameworkUtil.createFilter("(osgi.jaxrs.resource=" + ITest.class.getName() + ")");
		Object service = getService(filter, 5000);
		assertNotNull(service);
		ServiceReference<Object> ref = serviceRegistration.getReference();
		
		JerseyResourceInstanceFactory<ITest> factory = new JerseyResourceInstanceFactory<>(context, ITest.class, ref);
		assertEquals(0, factory.getCacheInstanceCount());
		ITest i1 = factory.provide();
		assertNull(i1);
		assertEquals(0, factory.getCacheInstanceCount());
		serviceRegistration.unregister();
	}
	
	<T> T getService(Class<T> clazz, long timeout) throws InterruptedException {
		ServiceTracker<T, T> tracker = new ServiceTracker<>(context, clazz, null);
		tracker.open();
		return tracker.waitForService(timeout);
	}
	
	<T> T getService(Filter filter, long timeout) throws InterruptedException {
		ServiceTracker<T, T> tracker = new ServiceTracker<>(context, filter, null);
		tracker.open();
		return tracker.waitForService(timeout);
	}

}