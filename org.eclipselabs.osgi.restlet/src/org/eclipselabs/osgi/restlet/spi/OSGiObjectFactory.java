package org.eclipselabs.osgi.restlet.spi;

import java.util.Collection;

import org.osgi.framework.BundleContext;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceObjects;
import org.osgi.framework.ServiceReference;
import org.osgi.service.component.ComponentContext;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.restlet.ext.jaxrs.InstantiateException;
import org.restlet.ext.jaxrs.ObjectFactory;

@Component(name="OSGiObjectFactory")
public class OSGiObjectFactory implements ObjectFactory {

	private volatile BundleContext bctx;
	
	@SuppressWarnings("unchecked")
	@Override
	public <T> T getInstance(Class<T> jaxRsClass) throws InstantiateException {
		if (bctx == null) {
			throw new InstantiateException("Cannot create instances because bundle context is not available");
		}
		try {
			Collection<ServiceReference<Object>> serviceReferences = bctx.getServiceReferences(Object.class, "(osgi.jaxrs.resource=" + jaxRsClass.getName() + ")");
			if (serviceReferences.isEmpty()) {
				System.out.println("No service found for class " + jaxRsClass.getName());
				return null;
			} else {
				ServiceReference<Object> reference = serviceReferences.iterator().next();
				Object scope = reference.getProperty("service.scope");
				Object instance = null;
				if ("prototype".equals(scope)) {
					ServiceObjects<Object> soInstance = bctx.getServiceObjects(reference);
					instance = soInstance.getService();
					System.out.println("Create prototyp instance " + instance);
				} else {
					instance = bctx.getService(reference);
					System.out.println("Create instance " + instance);
				}
				return (T)instance;
			}
		} catch (InvalidSyntaxException e) {
			throw new InstantiateException("Cannot find service for class " + jaxRsClass.getName(), e);
		}
	}
	
	@Activate
	public void activate(ComponentContext ctx) {
		bctx = ctx.getBundleContext();
	}

	@Deactivate
	public void deactivate() {
		bctx = null;
	}
}
