package org.eclipselabs.osgi.restlet.spi;

import javax.ws.rs.core.Application;

import org.eclipselabs.osgi.jaxrs.SampleAdminApplication;
import org.eclipselabs.osgi.jaxrs.SampleApplication;
import org.osgi.service.component.ComponentContext;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.log.LogService;
import org.restlet.Server;
import org.restlet.data.Protocol;
import org.restlet.ext.jaxrs.JaxRsApplication;
import org.restlet.ext.jaxrs.ObjectFactory;

@Component(name="RestletServer",immediate=true)
public class RestletComponent {
	
	@Reference
	private LogService logger;
	
	@Reference
	private ObjectFactory objectFactory;
	
	@Reference(target="(osgi.jaxrs.application=*)")
	private Application jaxrsApplication;
	
	private volatile org.restlet.Component serverComponent;
	private volatile org.restlet.Component serverComponent2;
//	private volatile Server server;
	
	@Activate
	public void activate(ComponentContext ctx) {
		System.out.println("Start Restlet server at port 8182");
		System.out.println("Start Restlet Admin server at port 8183");
		logger.log(LogService.LOG_ERROR, "Start Restlet Server");
		serverComponent = new org.restlet.Component();
		serverComponent2 = new org.restlet.Component();
		Server server = new Server(Protocol.HTTP, 8182);
		Server server2 = new Server(Protocol.HTTP, 8183);
//		server = new Server(Protocol.HTTP, 8182, new TestHandler());
		serverComponent.getServers().add(server);
		serverComponent2.getServers().add(server2);
		JaxRsApplication application = new JaxRsApplication(serverComponent.getContext().createChildContext());
		application.setObjectFactory(objectFactory);
		application.add(new SampleApplication());
		if (application != null) {
			System.out.println("Register whiteboard application");
			application.add(jaxrsApplication);
		}
		JaxRsApplication applicationAdmin = new JaxRsApplication(serverComponent2.getContext().createChildContext());
		applicationAdmin.add(new SampleAdminApplication());
		serverComponent.getDefaultHost().attach("/rest", application);
		serverComponent2.getDefaultHost().attach("/admin", applicationAdmin);
		try {
			serverComponent.start();
			serverComponent2.start();
//			server.start();
			System.out.println("Started Restlet server at port 8182 successfully try http://localhost:8181");
			System.out.println("Started Restlet admin server at port 8183 successfully try http://localhost:8181");
		} catch (Exception e) {
			System.out.println("Error starting Restlet server on port 8182 or admin server on port 8183"+  e.getMessage());
			e.printStackTrace();
		}
	}
	
	@Deactivate
	public void deactivate() {
		System.out.println("so Stop Restlet Server");
		logger.log(LogService.LOG_INFO, "Stop Restlet Server");
		if (serverComponent != null && serverComponent.isStarted()) {
//		if (server != null && server.isStarted()) {
			try {
				serverComponent.stop();
				serverComponent2.stop();
//				server.stop();
			} catch (Exception e) {
				System.out.println("Error stopping Restlet server "+  e.getMessage());
			}
		}
	}

}
