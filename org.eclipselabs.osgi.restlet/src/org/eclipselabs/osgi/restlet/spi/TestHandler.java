package org.eclipselabs.osgi.restlet.spi;

import org.restlet.Request;
import org.restlet.Response;
import org.restlet.Restlet;
import org.restlet.data.MediaType;

public class TestHandler extends Restlet {
	@Override
	public void handle(Request request, Response response) {
		response.setEntity("Hello world!", MediaType.TEXT_PLAIN);
	}

}
