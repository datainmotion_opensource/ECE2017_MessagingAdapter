package org.eclipselabs.osgi.restlet.spi;

import org.restlet.data.MediaType;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.Get;
import org.restlet.resource.ServerResource;

public class TestResource extends ServerResource {
	
	@Get
	public Representation sayHello() {
		String hello = "Hello Worls";
		return new StringRepresentation(hello, MediaType.TEXT_PLAIN);
	}

}
