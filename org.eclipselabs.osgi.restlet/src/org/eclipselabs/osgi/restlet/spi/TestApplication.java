package org.eclipselabs.osgi.restlet.spi;

import org.restlet.Application;
import org.restlet.Restlet;
import org.restlet.routing.Router;

public class TestApplication extends Application {
	
    public synchronized Restlet createInboundRoot() {

        Router router = new Router(getContext());

        router.attach("/test",TestResource.class );

        return router;

    }

}
